"""
Terminal Image Viewer
"""

__version__ = "0.2.0"
__license__ = "BSD"
__year__ = "2018"
__author__ = "Predrag Mandic"
__author_email__ = "info@nul.one"
__copyright__ = "Copyright {} {} <{}>".format(
    __year__, __author__, __author_email__)

